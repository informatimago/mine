;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               packages.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;
;;;;    XXX
;;;;
;;;;AUTHORS
;;;;    <PJB> Pascal Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2008-06-04 <PJB> Created.
;;;;BUGS
;;;;LEGAL
;;;;    GPL
;;;;
;;;;    Copyright Pascal Bourguignon 2008 - 2008
;;;;
;;;;    This program is free software; you can redistribute it and/or
;;;;    modify it under the terms of the GNU General Public License
;;;;    as published by the Free Software Foundation; either version
;;;;    2 of the License, or (at your option) any later version.
;;;;
;;;;    This program is distributed in the hope that it will be
;;;;    useful, but WITHOUT ANY WARRANTY; without even the implied
;;;;    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;;;;    PURPOSE.  See the GNU General Public License for more details.
;;;;
;;;;    You should have received a copy of the GNU General Public
;;;;    License along with this program; if not, write to the Free
;;;;    Software Foundation, Inc., 59 Temple Place, Suite 330,
;;;;    Boston, MA 02111-1307 USA
;;;;**************************************************************************

(defpackage "COM.INFORMATIMAGO.GAMES.MINE"
  (:use "COMMON-LISP")
  (:export "MINE"
           "TERMINAL-DISPLAY-BOARD" "TERMINAL-PLAYER")
  (:documentation "
The game of Mine.

Copyright Pascal Bourguignon 2008 - 2008

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version
2 of the License, or (at your option) any later version.
"))


(defpackage "COM.INFORMATIMAGO.GAMES.MINE-WEB-PLAYER"
  (:use "COMMON-LISP"
         "COM.INFORMATIMAGO.COMMON-LISP.HTML-GENERATOR.HTML")
  (:shadowing-import-from "COM.INFORMATIMAGO.COMMON-LISP.HTML-GENERATOR.HTML"
                          "MAP")
  (:export "PLAYER")
  (:documentation "
This program let a user play with mine thru a hunchentoot web page.

Copyright Pascal Bourguignon 2008 - 2008

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version
2 of the License, or (at your option) any later version.
"))


(defpackage "COM.INFORMATIMAGO.GAMES.MINE-PLAYER"
  (:use "COMMON-LISP")
  (:export "PLAYER" "PLAY")
  (:documentation "
This program plays Mine.

Copyright Pascal Bourguignon 2008 - 2008

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version
2 of the License, or (at your option) any later version.
"))


;;;; THE END ;;;;
