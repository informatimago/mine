;;;; -*- mode:lisp;coding:utf-8 -*-

(asdf:defsystem :com.informatimago.game.mine
    :name "MINE Game" 
    :description  "This is a web application implementing the MINE game."
    :author "<PJB> Pascal Bourguignon <pjb@informatimago.com"
    :version "0.0.2"
    :licence "GPL 2"
    :properties ((#:author-email                   . "pjb@informatimago.com")
                 (#:date                           . "Spring 2011")
                 ((#:albert #:output-dir)          . "../documentation/com.informatimago.game.mine/")
                 ((#:albert #:formats)             . ("docbook"))
                 ((#:albert #:docbook #:template)  . "book")
                 ((#:albert #:docbook #:bgcolor)   . "white")
                 ((#:albert #:docbook #:textcolor) . "black"))
    :depends-on ("split-sequence"
                 "hunchentoot"
                 "bordeaux-threads"
                 "com.informatimago.common-lisp")
    #+asdf-unicode :encoding #+asdf-unicode :utf-8
    :components ((:file "mine-packages")
                 (:file "mine"            :depends-on ("mine-packages"))
                 (:file "mine-web-player" :depends-on ("mine"))
                 (:file "mine-player"     :depends-on ("mine"))))


;;;; THE END ;;;;
