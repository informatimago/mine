#!/bin/bash
#*****************************************************************************
#FILE:               lisp.sh
#LANGUAGE:           bash
#SYSTEM:             POSIX
#USER-INTERFACE:     NONE
#DESCRIPTION
#    
#    Launches a lisp.
#    
#AUTHORS
#    <PJB> Pascal Bourguignon <pjb@informatimago.com>
#MODIFICATIONS
#    2006-10-19 <PJB> Renamed the dependencies directories adding 
#                     the IMPLEMENTATION-ID.
#    2006-09-17 <PJB> Created.
#BUGS
#LEGAL
#    Proprietary
#    
#    Copyright Andr� Thieme & Pascal Bourguignon 2006 - 2006
#    
#    All Rights Reserved.
#    
#    This program and its documentation constitute intellectual property 
#    of Pascal J. Bourguignon and is protected by the copyright laws of 
#    the European Union and other countries.
#*****************************************************************************
    
core="default"
impl="sbcl"
enco="ISO-8859-1"
# Sbcl uses LC_CTYPE environment variable for FFI conversions,
# and it doesn't take into account modifications (thru SB-POSIX:PUTENV)
# and I don't know any other way to set it in a running sbcl process.
# So we better set LC_CTYPE=en_US.ISO-8859-1 and we explicitely
# (SETF SB-IMPL::*DEFAULT-EXTERNAL-FORMAT* :UTF-8) for the file encoding.
# (SETF SB-ALIEN::*DEFAULT-C-STRING-EXTERNAL-FORMAT* :ISO-8859-1)

DEPSDIR="$( while [ $(pwd) != / ] ; do  if ls -d -1 deps-*-*-* >/dev/null 2>&1 ; then  pwd ; break ; else  cd .. ; fi ; done )"
if [ -z "$DEPSDIR" ] ; then
    DEPSDIR="$(cd $(dirname $0)/../../ ; pwd)"
fi


for arg ; do
    case "$arg" in
    help|-h|--help)
        echo "Usage: lisp.sh keyword..."
        echo "keyword can be one of:"
        echo "   sbcl        Selects sbcl. (This is the default)"
        echo "   clisp       Selects clisp"
        echo ""
        echo "   <project>   Selects a project core."
        echo ""
        echo "   <encoding>  <encoding> being one of the encoding names"
        echo "               as listed by iconv -l (without the //)"
        echo "               Selects this encoding (not all of them are "
        echo "               supported by sbcl!) Default: ISO-8859-1"
        exit 0
        ;;
    clisp|sbcl) impl="$arg" ;;
    *) 
        if { iconv -l | grep -q -s -i "^${arg}\(//\|$\)" ; } ; then
            enco="$arg"
        else
            if ls -d -1 "$DEPSDIR/deps-${arg}-${impl}-"* >/dev/null 2>&1 ; then
                core="$arg"
            else
                echo "Unknown keyword: ${arg}"
                exit 1
            fi
        fi
    esac
done



# We must add --sysinit /dev/null to prevent loading asdf-binary-location on gentoo.
# Perhaps we should use it always...
SBCL_OPTIONS=(--noinform --userinit /dev/null)
SBCL_EVAL=(--sysinit /dev/null --noprint --eval)
SBCL_IMAGE=(--core)

CLISP_OPTIONS=(-ansi -q -norc -K full -E "${enco}" -Eforeign ISO-8859-1 -on-error debug)
CLISP_EVAL=(-x)
CLISP_IMAGE=(-M)


#--- impl --- 
# Note: we use a specific path; in production we should use a specific version...
case "$impl" in
sbcl)
    if [ -x /usr/local/bin/sbcl ] ; then
        LISP=/usr/local/bin/sbcl
    else
        LISP=/usr/bin/sbcl
    fi
    ;;
clisp)
    if [ -x /usr/local/bin/clisp ] ; then
        LISP=/usr/local/bin/clisp
    else
        LISP=/usr/bin/clisp
    fi
    ;;
*)
    echo "Unimplemented implementation: ${impl}"
    exit 1
    ;;
esac


#--- enco ---
if [ "$impl" = "sbcl" ] ; then
    # I don't remember this variable being available in pre-1.0 SBCL...
    sbcl_has_ffi_encoding_p=$( ${LISP} ${SBCL_OPTIONS[@]} ${SBCL_EVAL[@]} '(progn (format t "~:[0~;1~]~%" (and (find-package "SB-ALIEN") (find-symbol "*DEFAULT-C-STRING-EXTERNAL-FORMAT*" "SB-ALIEN"))) (finish-output) (sb-ext:quit :unix-status 0))' 2>/dev/null )
    # SBCL doesn't seem to like heterogenous settings 
    # for these environment variables.
    unset LC_MONETARY
    unset LC_NUMERIC
    unset LC_MESSAGES
    unset LC_COLLATE
    unset LC_TIME
    if [ "$sbcl_has_ffi_encoding_p" = 1 ] ; then
        export LC_CTYPE=en_US.${enco}
    else
        export LC_CTYPE=en_US.ISO-8859-1
        echo "
Sbcl uses LC_CTYPE environment variable for FFI conversions,
and it doesn't take into account modifications (thru SB-POSIX:PUTENV)
and I don't know any other way to set it in a running sbcl process.
So we better set LC_CTYPE=en_US.ISO-8859-1 and we explicitely
(SETF SB-IMPL::*DEFAULT-EXTERNAL-FORMAT* :${enco}) for the file encoding.
The unfortunate consequence is that *STANDARD-INPUT* *STANDARD-OUTPUT* and
*TERMINAL-IO* are in ISO-8859-1.
"
#     if [ "${enco}" != "iso-8859-1" ] ; then
#         echo "If your source files are not encoded in ${enco}, it might be"
#         echo "prudent to: "
#         echo "(SETF SB-IMPL::*DEFAULT-EXTERNAL-FORMAT* :ISO-8859-1)"
#     fi
    fi
fi


#--- impl arguments --- 
case "$impl" in
sbcl)
    if [ "$sbcl_has_ffi_encoding_p" = 1 ] ; then
        LISP_OPTIONS=("${SBCL_OPTIONS[@]}" \
            --eval "(SETF SB-IMPL::*DEFAULT-EXTERNAL-FORMAT* :${enco} SB-ALIEN::*DEFAULT-C-STRING-EXTERNAL-FORMAT* :ISO-8859-1)")
    else
        LISP_OPTIONS=("${SBCL_OPTIONS[@]}" \
            --eval "(SETF SB-IMPL::*DEFAULT-EXTERNAL-FORMAT* :${enco})")
    fi
    LISP_EVAL=("${SBCL_EVAL[@]}")
    LISP_IMAGE=("${SBCL_IMAGE[@]}")
    ;;
clisp)
    LISP_OPTIONS=("${CLISP_OPTIONS[@]}")
    LISP_EVAL=("${CLISP_EVAL[@]}")
    LISP_IMAGE=("${CLISP_IMAGE[@]}")
    ;;
esac




#--- core ---
if [ "$core" = "default" ] ; then
    true
else
    DEPS=$("$LISP" "${LISP_OPTIONS[@]}" "${LISP_EVAL[@]}" "
;; We need the same processing as in dependencies.lisp and project.lisp
;; but project.lisp depends on the project...
(multiple-value-bind (result error)
    (ignore-errors
      (let ((*DEFAULT-VERSION*
             #+CLISP NIL
             #+SBCL  NIL
             #-(OR CLISP SBCL) (PROGN (WARN \"What default version to use in ~A?\"
                                            (LISP-IMPLEMENTATION-TYPE))
                                      :NEWEST))
            (*project-name* \""${core}"\")
            (*project-directory* #P\""${DEPSDIR}"/\"))
        (declare (special *default-version* *project-name* *project-directory*))
        (flet ((implementation-id ()
                 (flet ((first-word (text)
                          (let ((pos (position #\\space text)))
                            (remove #\\. (if pos
                                             (subseq text 0 pos)
                                             text)))))
                   (format nil \"~A-~A-~A\"
                           (first-word (lisp-implementation-type))
                           (first-word (lisp-implementation-version))
                           (first-word (machine-type))))))
          (namestring
           (merge-pathnames
            (make-pathname
             :directory (list :relative (format nil \"~:@(DEPS-~A-~A~)\"
                                                *project-name*
                                                (implementation-id)))
             :case :common :defaults *project-directory*)
            *project-directory* *default-version*)))))
  (if error
      (princ error  *error-output*)
      (princ result *standard-output*))
  (quit))
")
    echo "DEPS=${DEPS}"
    if [ -d "${DEPS}" ] ; then
        # IMPORTANT for SBCL: The core option must go after the other lisp options!
        LISP_OPTIONS=( "${LISP_IMAGE[@]}" "${DEPS}/${core}.core" "${LISP_OPTIONS[@]}" )
    else
        #echo "No dependencies for project ${core} with ${imid}"
        echo "Directory ${DEPS} inexistent."
        echo "Run the following to make it:"
        echo ""
        echo "  $0 $enco $impl"
        echo "  (load \"$(dirname $0)/../dependencies/dependencies.lisp\")"
        echo "  (load \"$(dirname $0)/../${core}/project.lisp\")"
        exit 1
    fi
fi
echo "$LISP" "${LISP_OPTIONS[@]}"
exec "$LISP" "${LISP_OPTIONS[@]}"


