;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               loader.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;
;;;;    Loads the mine web player.
;;;;
;;;;AUTHORS
;;;;    <PJB> Pascal Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2008-06-04 <PJB> Created.
;;;;BUGS
;;;;LEGAL
;;;;    GPL
;;;;
;;;;    Copyright Pascal Bourguignon 2008 - 2008
;;;;
;;;;    This program is free software; you can redistribute it and/or
;;;;    modify it under the terms of the GNU General Public License
;;;;    as published by the Free Software Foundation; either version
;;;;    2 of the License, or (at your option) any later version.
;;;;
;;;;    This program is distributed in the hope that it will be
;;;;    useful, but WITHOUT ANY WARRANTY; without even the implied
;;;;    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;;;;    PURPOSE.  See the GNU General Public License for more details.
;;;;
;;;;    You should have received a copy of the GNU General Public
;;;;    License along with this program; if not, write to the Free
;;;;    Software Foundation, Inc., 59 Temple Place, Suite 330,
;;;;    Boston, MA 02111-1307 USA
;;;;**************************************************************************


(in-package "COMMON-LISP-USER")

(let ((asdf:*central-registry*
       (cons (make-pathname :name nil :type nil :version nil
                            :defaults *load-pathname*)
             asdf:*central-registry*)))
  (ql:quickload :com.informatimago.game.mine))


(in-package "COM.INFORMATIMAGO.GAMES.MINE-WEB-PLAYER")


(defun ps ()
  #+sbcl
  (dolist (thread (sb-thread:list-all-threads))
    (format t "~40A ~:[dead ~;alive~]~%"
            (sb-thread:thread-name thread)
            (sb-thread:thread-alive-p thread)))
  (values))

(defun ns ()
  #+sbcl
  (sb-ext:run-program "/bin/netstat" (list "-tnpl") :output t)
  (values))


(defparameter *application-name*  "MINE")
(defparameter *application-port* 8007)
(start-server)
(ps)
(ns)


;;;; THE END ;;;;
